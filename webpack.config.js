const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  watch: true,
  devtool: 'source-map',
  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    compress: true,
    port: 9000,
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          configFile: path.resolve(__dirname, '.eslintrc.js'),
        },
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
    ],
  },
  entry: ['@babel/polyfill', './app.js'],
  output: {
    filename: 'deterson-health-records-app.js',
    path: path.resolve(__dirname, 'dist/js/'),
    publicPath: 'js',
  },
  plugins: [
    new HtmlWebPackPlugin({
      title: 'Deterson Health Records',
      hash: true,
      template: './index.html',
      filename: `${path.resolve(__dirname, 'dist/')}/index.html`,
    }),
    new CopyWebpackPlugin([
      { from: 'src/images', to: `${path.resolve(__dirname, 'dist/')}/images` },
    ]),
  ],
};
