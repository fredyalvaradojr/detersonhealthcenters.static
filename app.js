import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './src/store';
import IndexTemplate from './src/components/templates/IndexTemplate';

const root = document.getElementById('deterson-health-records-app');
const returnedStore = store();

ReactDOM.render(
  <Provider store={returnedStore}>
    <IndexTemplate />
  </Provider>,
  root,
);
