# Deterson Health Records App

This app has been developed to allow a user to view a list of doctors close to their area.

It has been built using React, Redux, Emotion (CSS-in-JS), and Webpack. The project can be run locally but is dependent on an API Server.

> ### Please follow step-by-step in order to get the App running properly. There are two seperate installations that need to take place in order to get this application up and running. Please make sure you install both the API server and application.

> The Production version is currently hosted at Firebase: https://det-helth-ui.firebaseapp.com/

## Install the API Server

#### Step 1: Download and Install Node

```bash
  * https://nodejs.org/en/download/current/
  * run installer
```

#### Step 2: Install DetersonHealthCenters.node API Server app

```bash
  * git clone https://fredyalvaradojr@bitbucket.org/fredyalvaradojr/detersonhealthcenters.node.git
  * cd detersonhealthcenters.node
  * npm install
  * npm start
```

#### Step 3: Start API Server

```bash
  * npm start
```

#### Step 4: Test doctors API Endpoint

```bash
  * http://localhost:3001/doctors
```

## Install Deterson Health Records App

#### Step 1: Install Application

```bash
  * git clone https://fredyalvaradojr@bitbucket.org/fredyalvaradojr/detersonhealthcenters.static.git
  * cd detersonhealthcenters.static
  * npm install
```

#### Step 2: Run Application

```bash
  * npm run build:dev:local
```

#### Step 3: Visit the Local App

```bash
  * http://localhost:9000/
```
