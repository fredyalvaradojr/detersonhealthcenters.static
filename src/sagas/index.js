import { takeEvery, all, put } from 'redux-saga/effects';

import { GET_SEARCH_RESULTS, loadSearchResultsSucceed } from './../actions';

import { getDoctorResultsApi } from './../services';

export function* getSearchResults() {
  const data = yield getDoctorResultsApi();
  const { results } = data;
  yield put(loadSearchResultsSucceed(results));
}

export function* watchSagas() {
  yield all([yield takeEvery(GET_SEARCH_RESULTS, getSearchResults)]);
}
