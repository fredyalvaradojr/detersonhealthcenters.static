import { createSelector } from 'reselect';

export const getSelectedGenderSelector = createSelector(
  state => state.detersonApp.gender,
  gender => gender,
);
