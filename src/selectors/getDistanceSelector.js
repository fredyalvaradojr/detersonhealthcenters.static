import { createSelector } from 'reselect';

export const getDistanceSelector = createSelector(
  state => state.detersonApp.distance,
  distance => distance,
);
