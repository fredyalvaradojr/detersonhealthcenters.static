import { filterByDistanceSelector } from './filterByDistanceSelector';

describe('Given some state', () => {
  let state;
  beforeEach(() => {
    state = {
      detersonApp: { distance: '30' },
    };
  });

  describe('When selecting the distance', () => {
    let result;
    beforeEach(() => {
      result = filterByDistanceSelector(state);
    });

    it('Then return the distance avaiable', () => {
      console.debug('result', result);
      expect(result).toBe('30');
    });
  });
});
