import { createSelector } from 'reselect';

export const searchResultsSelector = createSelector(
  state => state.detersonApp.results,
  results => results,
);
