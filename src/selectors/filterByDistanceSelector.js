import { createSelector } from 'reselect';

export const filterByDistanceSelector = createSelector(
  state => state.detersonApp.distance,
  distance => distance,
);
