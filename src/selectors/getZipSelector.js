import { createSelector } from 'reselect';

export const getZipSelector = createSelector(
  state => state.detersonApp.zip,
  zip => zip,
);
