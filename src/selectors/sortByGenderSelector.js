import { createSelector } from 'reselect';

export const sortByGenderSelector = createSelector(
  state => state.detersonApp.gender,
  gender => gender,
);
