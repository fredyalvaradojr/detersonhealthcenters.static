import { createSelector } from 'reselect';

export const loadingSearchResultsSelector = createSelector(
  state => state.detersonApp.loadingResults,
  loadingResults => loadingResults,
);
