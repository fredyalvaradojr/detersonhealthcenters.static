import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducer from './../reducers';
import { watchSagas } from './../sagas';

const store = () => {
  const sagaMiddleware = createSagaMiddleware();
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const configuredStore = createStore(reducer, composeEnhancers(applyMiddleware(sagaMiddleware)));

  sagaMiddleware.run(watchSagas);

  return configuredStore;
};

export default store;
