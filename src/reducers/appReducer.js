import {
  LOAD_SEARCH_RESULTS_SUCCEED,
  SET_DISTANCE,
  SET_ZIP,
  SET_GENDER,
  SET_LOADING_SEARCH_RESULTS,
} from '../actions';
import { resultsInitState } from '../util';

export const detersonApp = (state = resultsInitState, action) => {
  const { type } = action;
  if (type === LOAD_SEARCH_RESULTS_SUCCEED) {
    return { ...state, results: action.results, loadingResults: false };
  }

  if (type === SET_DISTANCE) {
    return { ...state, distance: action.distance };
  }

  if (type === SET_ZIP) {
    return { ...state, zip: action.zip };
  }

  if (type === SET_GENDER) {
    return { ...state, gender: action.gender };
  }

  if (type === SET_LOADING_SEARCH_RESULTS) {
    return { ...state, loadingResults: action.status };
  }

  return state;
};
