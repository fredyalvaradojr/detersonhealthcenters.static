import { combineReducers } from 'redux';
import { detersonApp } from './appReducer';

export default combineReducers({
  detersonApp,
});
