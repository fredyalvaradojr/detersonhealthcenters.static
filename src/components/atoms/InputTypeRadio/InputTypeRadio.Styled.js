import styled from '@emotion/styled';

export const Label = styled.label`
  display: flex;
  align-items: center;
`;

export const LabelTitle = styled.span`
  font-size: 1em;
  color: #25aae1;
  padding-left: 0.625em;
`;

export const FauxInput = styled.span`
  display: inline-block;
  position: relative;
  background: #fff;
  border: 0.0625em solid #b4aea4;
  border-radius: 0.1875em;
  width: 0.8125em;
  height: 0.8125em;

  &:after {
    content: ' ';
    position: absolute;
    width: 0.4375em;
    height: 0.4375em;
    top: 0.125em;
    left: 0.125em;
    z-index: 1;
    background: #25aae1;
    opacity: 0;
    border-radius: 0.1875em;
  }
`;

export const Input = styled.input`
  position: absolute;
  opacity: 0;
  cursor: pointer;

  &:checked ~ ${FauxInput} {
    &:after {
      opacity: 1;
    }
  }
`;
