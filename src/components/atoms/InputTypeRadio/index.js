import React from 'react';
import PropTypes from 'prop-types';
import { noop } from '@babel/types';
import * as S from './InputTypeRadio.Styled';

const InputTypeRadio = ({
  title, value, group, selected, callback,
}) => (
  <S.Label>
    <S.Input
      type="radio"
      value={value}
      name={group}
      onChange={callback}
      checked={selected === value || false}
    />
    <S.FauxInput />
    <S.LabelTitle>{title}</S.LabelTitle>
  </S.Label>
);

InputTypeRadio.propTypes = {
  title: PropTypes.string,
  value: PropTypes.string,
  callback: PropTypes.func,
  group: PropTypes.string,
  selected: PropTypes.string,
};

InputTypeRadio.defaultProps = {
  title: 'This needs a title.',
  value: null,
  callback: noop,
  group: '',
  selected: '',
};
export default InputTypeRadio;
