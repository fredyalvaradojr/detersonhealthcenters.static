import React from 'react';
import PropTypes from 'prop-types';
import { noop } from '@babel/types';
import * as S from './InputTypeTel.Styled';

const InputTypeTel = ({
  forConnect, styleName, connectedValue, callback,
}) => (
  <S.InputTypeTel
    className={styleName}
    type="tel"
    name={forConnect}
    id={forConnect}
    value={connectedValue}
    onChange={callback}
    min="5"
    max="5"
  />
);

InputTypeTel.propTypes = {
  forConnect: PropTypes.string,
  styleName: PropTypes.string,
  connectedValue: PropTypes.string.isRequired,
  callback: PropTypes.func,
};

InputTypeTel.defaultProps = {
  forConnect: 'This needs to be connected.',
  styleName: null,
  callback: noop,
};
export default InputTypeTel;
