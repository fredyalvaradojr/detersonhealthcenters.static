import styled from '@emotion/styled';

export const InputTypeTel = styled.input`
  border: 0.0625em solid #dbd5cc;
  background: #fff;
  border-radius: 0.1875em;
  font-size: 1.125em;
  padding: 0.3em 0.5em;
`;
