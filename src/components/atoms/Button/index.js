import React from 'react';
import PropTypes from 'prop-types';
import { noop } from '@babel/types';
import * as S from './Button.Styled';

const Button = ({
  type, value, styleName, callback,
}) => (
  <S.Button className={`${type} ${styleName}`} onClick={callback}>
    {value}
  </S.Button>
);

Button.propTypes = {
  type: PropTypes.string,
  value: PropTypes.string,
  styleName: PropTypes.string,
  callback: PropTypes.func,
};

Button.defaultProps = {
  type: '',
  value: 'What kind of button?',
  styleName: null,
  callback: noop,
};

export default Button;
