import styled from '@emotion/styled';

export const Button = styled.button`
  font-size: 1.125em;
  color: #000;
  font-weight: bold;
  background: #ccc;
  padding: 0 1em;
  border-radius: 0.1875em;
  cursor: pointer;

  &.submit {
    background: #5ab44b;
    color: #fff;
  }
`;
