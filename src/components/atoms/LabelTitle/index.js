import React from 'react';
import PropTypes from 'prop-types';
import * as S from './LabelTitle.Styled';

const LabelTitle = ({ title }) => <S.LabelTitle>{title}</S.LabelTitle>;

LabelTitle.propTypes = {
  title: PropTypes.string,
};

LabelTitle.defaultProps = {
  title: 'This needs a title',
};

export default LabelTitle;
