import styled from '@emotion/styled';

export const LabelTitle = styled.span`
  font-size: 1.0625em;
  color: #695e4a;
`;
