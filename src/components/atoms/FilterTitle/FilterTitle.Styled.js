import styled from '@emotion/styled';

export const FilterTitle = styled.h2`
  font-size: 1.25em;
  color: #006595;
  margin-bottom: 0.9em;
`;
