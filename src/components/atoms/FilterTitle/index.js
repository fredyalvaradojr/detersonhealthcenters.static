import React from 'react';
import PropTypes from 'prop-types';
import * as S from './FilterTitle.Styled';

const FilterTitle = ({ title }) => <S.FilterTitle>{title}</S.FilterTitle>;

FilterTitle.propTypes = {
  title: PropTypes.string,
};

FilterTitle.defaultProps = {
  title: 'This needs a title.',
};
export default FilterTitle;
