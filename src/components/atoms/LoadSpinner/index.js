import React from 'react';
import * as S from './LoadSpinner.Styled';

const LoadSpinner = () => (
  <S.LoadSpinner className="css-ailvj2">
    <S.Ball />
    <S.BallAlt />
  </S.LoadSpinner>
);

export default LoadSpinner;
