import { keyframes } from '@emotion/core';
import styled from '@emotion/styled';

export const LoadSpinner = styled.div`
  position: relative;
  width: 60px;
  height: 60px;
  display: inline-block;
  margin-bottom: 3em;
`;

export const zoom = keyframes`
  0%, 100% {
    transform: scale(0);
  }
  50% {
      transform: scale(1);
  }
`;

export const Ball = styled.div`
  position: absolute;
  height: 60px;
  width: 60px;
  background-color: rgb(54, 215, 183);
  opacity: 0.6;
  top: 0px;
  left: 0px;
  border-radius: 100%;
  animation: 2.1s ease-in-out 1s infinite normal none running ${zoom};
`;

export const BallAlt = styled.div`
  position: absolute;
  height: 60px;
  width: 60px;
  background-color: rgb(54, 215, 183);
  opacity: 0.6;
  top: 0px;
  left: 0px;
  border-radius: 100%;
  animation: 2.1s ease-in-out 0s infinite normal none running ${zoom};
`;
