import styled from '@emotion/styled';

export const GenderFilterList = styled.ul``;

export const GenderFilterListItem = styled.li`
  margin-bottom: 0.625em;

  @media (min-width: 48em) {
    margin-bottom: 0.9375em;
  }
`;
