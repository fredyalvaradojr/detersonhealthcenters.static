import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setGender } from '../../../actions';
import { getGenderData } from '../../../util';
import { getSelectedGenderSelector } from '../../../selectors';
import InputTypeRadio from '../../atoms/InputTypeRadio';
import * as S from './GenderSort.Styled';

const GenderSort = ({ selectedGender, callSetGenderSort }) => {
  const callbackToggleGender = (e) => {
    const { value: newGenderSelection } = e.target;
    callSetGenderSort(newGenderSelection);
  };
  const genderFilters = getGenderData(callbackToggleGender);

  return (
    <S.GenderFilterList>
      {genderFilters.map((genderFilterItem) => {
        const {
          title, value, group, callback,
        } = genderFilterItem;
        return (
          <S.GenderFilterListItem key={value}>
            <InputTypeRadio
              title={title}
              selected={selectedGender}
              value={value}
              group={group}
              callback={callback}
            />
          </S.GenderFilterListItem>
        );
      })}
    </S.GenderFilterList>
  );
};

GenderSort.propTypes = {
  callSetGenderSort: PropTypes.func.isRequired,
  selectedGender: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
  const selectedGender = getSelectedGenderSelector(state);
  return {
    selectedGender,
  };
};

const mapDispatchToProps = dispatch => ({
  callSetGenderSort: gender => dispatch(setGender(gender)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GenderSort);
