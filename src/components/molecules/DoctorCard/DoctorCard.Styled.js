import styled from '@emotion/styled';

export const DoctorCard = styled.div`
  margin-bottom: 2.25em;
  display: flex;
  padding: 0 0 2.25em;
  border-bottom: 0.125em solid #f2f7fa;

  @media (min-width: 48em) {
    padding: 1.625em 0.375em;
  }

  @media (min-width: 62em) {
    max-width: 59.375em;
    margin: 0 auto 3.125em;
  }
`;

export const ImageContainer = styled.div`
  padding-bottom: 0.625em;
  flex: 0 0 6.125em;
  margin-right: 1.25em;
`;

export const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 1 auto;

  @media (min-width: 48em) {
    flex-direction: row;
  }
`;

export const ContentNameContainer = styled.section`
  @media (min-width: 48em) {
    flex: 2;
  }
  @media (min-width: 64em) {
    flex: 3;
  }
`;

export const DoctorName = styled.h3`
  font-size: 1.25em;
  color: #25aae1;
  font-weight: 700;
  margin-bottom: 0.4em;

  &.no-specialties {
    margin-bottom: 1em;
  }
`;

export const DoctorNameLink = styled.a`
  color: #25aae1;
  text-decoration: none;
`;

export const SpecialitiesList = styled.ul`
  margin-bottom: 1em;
`;

export const SpecialitiesListItem = styled.li`
  margin-bottom: 0.2em;
`;

export const Specialty = styled.span`
  font-size: 1.25em;
  color: #000;
  font-weight: 400;
`;

export const LocationList = styled.ul`
  flex: 1;
`;

export const LocationItem = styled.li`
  margin-bottom: 1.25em;

  &:last-child {
    margin-bottom: 0;
  }
`;

export const LocationName = styled.h4`
  margin-bottom: 0.2em;
  font-size: 1em;
  color: #25aae1;
  font-weight: 400;
`;

export const LocationNameLink = styled.a`
  color: #25aae1;
  text-decoration: none;
`;

export const LocationDistance = styled.p`
  color: #000;
  font-weight: 700;
  text-transform: uppercase;
`;
