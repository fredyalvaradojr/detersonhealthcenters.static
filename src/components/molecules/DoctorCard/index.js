import React from 'react';
import PropTypes from 'prop-types';
import * as S from './DoctorCard.Styled';

const DoctorCard = ({
  fullName, image, locations, specialties, url,
}) => {
  const loadImage = image.length > 0 || false;
  const loadSpecialties = specialties.length > 0 || false;
  const loadLocations = locations.length > 0 || false;

  return (
    <S.DoctorCard>
      <S.ImageContainer>
        <img src={loadImage ? image : '../images/avatar.png'} alt={fullName} />
      </S.ImageContainer>
      <S.ContentContainer>
        <S.ContentNameContainer>
          <S.DoctorName className={!loadSpecialties ? 'no-specialties' : null}>
            <S.DoctorNameLink href={url}>{fullName}</S.DoctorNameLink>
          </S.DoctorName>
          {loadSpecialties ? (
            <S.SpecialitiesList>
              {specialties.map(specialty => (
                <S.SpecialitiesListItem key={specialty}>
                  <S.Specialty>{specialty}</S.Specialty>
                </S.SpecialitiesListItem>
              ))}
            </S.SpecialitiesList>
          ) : null}
        </S.ContentNameContainer>
        {loadLocations ? (
          <S.LocationList>
            {locations.map((location) => {
              const { name, distance, url: locationUrl } = location;

              return (
                <S.LocationItem key={name}>
                  <S.LocationName>
                    <S.LocationNameLink href={locationUrl}>{name}</S.LocationNameLink>
                  </S.LocationName>
                  <S.LocationDistance>
                    {`${Math.round(distance)} ${Math.round(distance) > 1 ? 'miles' : 'mile'}`}
                  </S.LocationDistance>
                </S.LocationItem>
              );
            })}
          </S.LocationList>
        ) : null}
      </S.ContentContainer>
    </S.DoctorCard>
  );
};

DoctorCard.propTypes = {
  fullName: PropTypes.string.isRequired,
  image: PropTypes.string,
  locations: PropTypes.array,
  specialties: PropTypes.array,
  url: PropTypes.string.isRequired,
};

DoctorCard.defaultProps = {
  image: '',
  locations: [],
  specialties: [],
};

export default DoctorCard;
