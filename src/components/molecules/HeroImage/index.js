import React from 'react';
import * as S from './HeroImage.Styled';

const HeroImage = () => (
  <S.HeroImage>
    <S.MainHeroImage src="../images/hospital.png" alt="Doctor's Office Building" />
  </S.HeroImage>
);

export default HeroImage;
