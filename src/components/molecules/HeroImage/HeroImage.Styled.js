import styled from '@emotion/styled';

export const HeroImage = styled.div`
  margin-bottom: 1.875em;
  overflow: hidden;
  text-align: center;
`;

export const MainHeroImage = styled.img`
  position: relative;
  left: -40%;
  max-height: 18em;
  width: auto;

  @media (min-width: 48em) {
    left: auto;
    max-height: inherit;
    width: 100%;
  }

  @media (min-width: 62em) {
    width: 59.375em;
  }
`;
