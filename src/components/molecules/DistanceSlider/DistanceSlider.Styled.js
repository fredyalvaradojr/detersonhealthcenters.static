import styled from '@emotion/styled';

export const InputTypeRange = styled.input`
  -webkit-appearance: none;
  width: 100%;
  margin: 8.5px 0;

  &:focus {
    outline: none;
  }
  &::-webkit-slider-runnable-track {
    width: 100%;
    height: 5px;
    cursor: pointer;
    box-shadow: 0px 0px 0px rgba(0, 0, 0, 0), 0px 0px 0px rgba(13, 13, 13, 0);
    background: #e4e1dc;
    border-radius: 0px;
    border: 0px solid rgba(1, 1, 1, 0);
  }
  &::-webkit-slider-thumb {
    box-shadow: 0px 0px 0px rgba(0, 0, 0, 0), 0px 0px 0px rgba(13, 13, 13, 0);
    border: 0px solid rgba(0, 0, 0, 0);
    height: 22px;
    width: 7px;
    border-radius: 0px;
    background: #25aae1;
    cursor: pointer;
    -webkit-appearance: none;
    margin-top: -8.5px;
  }
  &:focus::-webkit-slider-runnable-track {
    background: #e4e1dc;
  }
  &::-moz-range-track {
    width: 100%;
    height: 5px;
    cursor: pointer;
    box-shadow: 0px 0px 0px rgba(0, 0, 0, 0), 0px 0px 0px rgba(13, 13, 13, 0);
    background: #e4e1dc;
    border-radius: 0px;
    border: 0px solid rgba(1, 1, 1, 0);
  }
  &::-moz-range-thumb {
    box-shadow: 0px 0px 0px rgba(0, 0, 0, 0), 0px 0px 0px rgba(13, 13, 13, 0);
    border: 0px solid rgba(0, 0, 0, 0);
    height: 22px;
    width: 7px;
    border-radius: 0px;
    background: #25aae1;
    cursor: pointer;
  }
  &::-ms-track {
    width: 100%;
    height: 5px;
    cursor: pointer;
    background: transparent;
    border-color: transparent;
    color: transparent;
  }
  &::-ms-fill-lower {
    background: #e4e1dc;
    border: 0px solid rgba(1, 1, 1, 0);
    border-radius: 0px;
    box-shadow: 0px 0px 0px rgba(0, 0, 0, 0), 0px 0px 0px rgba(13, 13, 13, 0);
  }
  &::-ms-fill-upper {
    background: #e4e1dc;
    border: 0px solid rgba(1, 1, 1, 0);
    border-radius: 0px;
    box-shadow: 0px 0px 0px rgba(0, 0, 0, 0), 0px 0px 0px rgba(13, 13, 13, 0);
  }
  &::-ms-thumb {
    box-shadow: 0px 0px 0px rgba(0, 0, 0, 0), 0px 0px 0px rgba(13, 13, 13, 0);
    border: 0px solid rgba(0, 0, 0, 0);
    height: 22px;
    width: 7px;
    border-radius: 0px;
    background: #25aae1;
    cursor: pointer;
  }
  &:focus::-ms-fill-lower {
    background: #e4e1dc;
  }
  &:focus::-ms-fill-upper {
    background: #e4e1dc;
  }
`;

export const InputTypeRangeMarks = styled.div`
  display: flex;
  width: 103%;
  justify-content: space-between;
  color: #858585;
  position: relative;
  margin: 0.5em 0 0 -0.0625em;
  position: relative;
  padding-bottom: 1.875em;
`;

export const Option = styled.span`
  font-size: 0.75em;
  font-weight: 700;
  margin: 0;
  padding: 0;
  position: absolute;
  left: calc(18.1% * ${props => props.number});

  @media (min-width: 768px) {
    left: calc(18.3% * ${props => props.number});
  }
`;

export const DistanceOutput = styled.p`
  color: #000;
  font-size: 0.75em;
  font-weight: 700;
  text-transform: uppercase;
`;
