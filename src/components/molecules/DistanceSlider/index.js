import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setDistance } from '../../../actions';
import { getZipSelector, getDistanceSelector } from '../../../selectors';
import * as S from './DistanceSlider.Styled';

const DistanceSlider = ({ callSetDistance, zip, distance }) => {
  const editedDistance = parseFloat(distance) === 30 ? 'All' : distance;
  const updateDistanceRange = (e) => {
    const { value } = e.target;
    callSetDistance(value);
  };

  return (
    <React.Fragment>
      <S.InputTypeRange
        id="slider1"
        type="range"
        min="5"
        max="30"
        step="5"
        value={distance}
        list="distanceMarks"
        onChange={updateDistanceRange}
      />
      <S.InputTypeRangeMarks id="distanceMarks">
        <S.Option number="0" value="5">
          5
        </S.Option>
        <S.Option number={1} value="10">
          10
        </S.Option>
        <S.Option number={2} value="15">
          15
        </S.Option>
        <S.Option number={3} value="20">
          20
        </S.Option>
        <S.Option number={4} value="25">
          25
        </S.Option>
        <S.Option number={5} value="all">
          All
        </S.Option>
      </S.InputTypeRangeMarks>
      <S.DistanceOutput>{`Current: ${editedDistance} miles from ${zip}`}</S.DistanceOutput>
    </React.Fragment>
  );
};

DistanceSlider.propTypes = {
  callSetDistance: PropTypes.func.isRequired,
  zip: PropTypes.string.isRequired,
  distance: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
  const zip = getZipSelector(state);
  const distance = getDistanceSelector(state);

  return {
    zip,
    distance,
  };
};

const mapDispatchToProps = dispatch => ({
  callSetDistance: distance => dispatch(setDistance(distance)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DistanceSlider);
