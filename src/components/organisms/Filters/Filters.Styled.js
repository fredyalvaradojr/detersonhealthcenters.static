import styled from '@emotion/styled';

export const Filters = styled.div`
  margin-bottom: 2em;
  display: flex;
  padding: 0 1em;

  @media (min-width: 48em) {
    margin-bottom: 2.5em;
    padding: 0 1.5em;
    flex-direction: column;
    flex: 0 0 12.0625em;
    align-self: flex-start;
  }
`;

export const DistanceContainer = styled.div`
  flex: 1;
  padding-right: 1em;
  border-right: 0.125em solid #f2f7fa;
  padding: 0.5em 1em 0.5em 0;

  @media (min-width: 48em) {
    padding-bottom: 1.75em;
    margin: 0 0 1.75em;
    border-right: 0;
    border-bottom: 0.125em solid #f2f7fa;
  }
`;

export const GenderContainer = styled.div`
  flex: 1;
  padding: 0.5em 0 0.5em 1rem;

  @media (min-width: 48em) {
    padding-left: 0;
  }
`;
