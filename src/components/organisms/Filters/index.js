import React from 'react';
import * as S from './Filters.Styled';
import FilterTitle from '../../atoms/FilterTitle';
import DistanceSlider from '../../molecules/DistanceSlider';
import GenderSort from '../../molecules/GenderSort';

const Filters = () => (
  <S.Filters>
    <S.DistanceContainer>
      <FilterTitle title="Distance" />
      <DistanceSlider />
    </S.DistanceContainer>
    <S.GenderContainer>
      <FilterTitle title="Gender" />
      <GenderSort />
    </S.GenderContainer>
  </S.Filters>
);

export default Filters;
