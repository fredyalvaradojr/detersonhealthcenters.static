import styled from '@emotion/styled';

export const DoctorSearch = styled.div`
  margin-bottom: 3.125em;
  display: flex;
  flex-direction: column;
  background: #f8f8f8;
  padding: 1.125em 1em 1.325em;

  @media (min-width: 48em) {
    padding: 1.625em 1.5em;
  }

  @media (min-width: 62em) {
    max-width: 59.375em;
    margin: 0 auto 3.125em;
  }
`;

export const Label = styled.label`
  padding-bottom: 0.625em;
`;

export const InputSubmitContainer = styled.section`
  display: flex;

  .docsearch--input {
    flex: 0 1 auto;
    min-width: 0;
    margin-right: 1em;

    @media (min-width: 48em) {
      flex: 1;
    }
  }

  .docsearch--button {
    height: 100%;
  }
`;

export const ButtonContainer = styled.section`
  flex: 1;
  display: flex;
  justify-content: flex-end;
`;

export const Error = styled.p`
  color: #cc0303;
  font-size: 0.6875em;
  margin-bottom: 0.625em;
  position: absolute;
  bottom: -1.5em;
`;
