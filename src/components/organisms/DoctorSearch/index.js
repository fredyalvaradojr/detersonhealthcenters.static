import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as S from './DoctorSearch.Styled';
import LabelTitle from '../../atoms/LabelTitle';
import InputTypeTel from '../../atoms/InputTypeTel';
import Button from '../../atoms/Button';

const DoctorSearch = ({ callback }) => {
  const [zipCodeInput, setZipCodeInput] = useState('');
  const [fixZipCode, setFixZipCode] = useState(false);
  const updateZipCodeInput = (e) => {
    const { value } = e.target;
    const setErrorMessageStatus = value.length > 5 || false;
    setFixZipCode(setErrorMessageStatus);
    setZipCodeInput(value);
  };
  const validateForm = () => {
    if (zipCodeInput.length !== 5) {
      setFixZipCode(true);
      return;
    }

    callback(zipCodeInput);
  };

  return (
    <S.DoctorSearch id="DoctorSearch">
      <S.Label htmlFor="SearchBox">
        <LabelTitle title="Zip Code" />
      </S.Label>
      <S.InputSubmitContainer>
        {fixZipCode ? <S.Error>* Please add a valid zipcode.</S.Error> : null}
        <InputTypeTel
          connectedValue={zipCodeInput}
          callback={updateZipCodeInput}
          styleName="docsearch--input"
          forConnect="SearchBox"
        />
        <S.ButtonContainer>
          <Button
            styleName="docsearch--button"
            type="submit"
            value="Search"
            callback={validateForm}
          />
        </S.ButtonContainer>
      </S.InputSubmitContainer>
    </S.DoctorSearch>
  );
};

DoctorSearch.propTypes = {
  callback: PropTypes.func.isRequired,
};

export default DoctorSearch;
