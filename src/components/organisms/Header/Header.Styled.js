import styled from '@emotion/styled';

export const Header = styled.header`
  background: url('https://firebasestorage.googleapis.com/v0/b/det-helth-ui.appspot.com/o/gradient.png?alt=media&token=57bd58b9-c33d-418b-b3f9-75760c315868')
    center top repeat-x;
  margin-bottom: 1.875em;
  padding-top: 0.5625em;
  box-shadow: 0 8px 5px -5px rgba(59, 59, 59, 0.09);
  text-align: center;
`;

export const HeaderLogo = styled.img`
  width: 6.8125em;
  height: auto;
  display: inline-block;
  padding: 0.625em 0;
`;
