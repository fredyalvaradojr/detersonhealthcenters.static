import React from 'react';
import * as S from './Header.Styled';

const Header = () => (
  <S.Header>
    <a href="/">
      <S.HeaderLogo src="../images/logo.svg" alt="Deterson Health Centers" />
    </a>
  </S.Header>
);

export default Header;
