import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as S from './SearchResults.Styled';
import DoctorCard from '../../molecules/DoctorCard';
import { searchResultsProjector } from '../../../projectors/searchResultsProjector';

const SearchResults = ({ searchResults, searchResultsCount }) => (
  <S.SearchResults>
    <S.SearchResultsHeader>
        Total Results:
      <S.ResultCount>{searchResultsCount}</S.ResultCount>
    </S.SearchResultsHeader>
    <ul>
      {searchResults.map(result => (
        <li key={result.fullName}>
          <DoctorCard {...result} />
        </li>
      ))}
    </ul>
  </S.SearchResults>
);

SearchResults.propTypes = {
  searchResults: PropTypes.array.isRequired,
  searchResultsCount: PropTypes.number.isRequired,
};

const mapStateToProps = (state) => {
  const searchResults = searchResultsProjector(state);
  const searchResultsCount = searchResults.length;

  return { searchResults, searchResultsCount };
};

export default connect(mapStateToProps)(SearchResults);
