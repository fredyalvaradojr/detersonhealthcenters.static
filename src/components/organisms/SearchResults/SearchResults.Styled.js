import styled from '@emotion/styled';

export const SearchResults = styled.div`
  padding: 0 1em;
  margin-bottom: 1.5em;

  @media (min-width: 48em) {
    flex: 0 1 auto;
    margin-left: 3.125em;
  }

  @media (min-width: 64em) {
    flex: 1 1 auto;
  }
`;

export const SearchResultsHeader = styled.h1`
  font-size: 1.375em;
  color: #858585;
  margin-bottom: 1.0625em;

  @media (min-width: 48em) {
    margin-bottom: 2.125em;
  }
`;

export const ResultCount = styled.span`
  margin-left: 0.375em;
`;
