import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as S from './IndexTemplate.Styled';
import { getSearchResults, setZip, setLoadingSearchResults } from '../../../actions';
import Header from '../../organisms/Header';
import HeroImage from '../../molecules/HeroImage';
import DoctorSearch from '../../organisms/DoctorSearch';
import Filters from '../../organisms/Filters';
import SearchResults from '../../organisms/SearchResults';
import { searchResultsSelector, loadingSearchResultsSelector } from '../../../selectors';
import LoadSpinner from '../../atoms/LoadSpinner';

const IndexTemplate = ({
  ShowSearchResults,
  callSearchResults,
  callLoadingSearchResults,
  loadingSearchResults,
}) => {
  const zipLoadSearchResults = (zip) => {
    callLoadingSearchResults(true);
    callSearchResults(zip);
  };

  useEffect(() => {
    const scrollTo = document.getElementById('DoctorSearch');
    scrollTo.scrollIntoView({ behavior: 'smooth' });
  }, [ShowSearchResults]);

  return (
    <S.IndexTemplate>
      <Header />
      <HeroImage />
      <DoctorSearch callback={zipLoadSearchResults} />
      {loadingSearchResults ? (
        <S.LoadSpinnerContainer>
          <LoadSpinner />
        </S.LoadSpinnerContainer>
      ) : null}
      {ShowSearchResults ? (
        <S.FilterResultsContainer>
          <Filters />
          <SearchResults />
        </S.FilterResultsContainer>
      ) : null}
    </S.IndexTemplate>
  );
};

IndexTemplate.propTypes = {
  loadingSearchResults: PropTypes.bool.isRequired,
  ShowSearchResults: PropTypes.bool.isRequired,
  callSearchResults: PropTypes.func.isRequired,
  callLoadingSearchResults: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  const loadingSearchResults = loadingSearchResultsSelector(state);
  const searchResults = searchResultsSelector(state);
  const ShowSearchResults = searchResults.length > 0 || false;
  return { ShowSearchResults, loadingSearchResults };
};

const mapDispatchToProps = dispatch => ({
  callSearchResults: (zip) => {
    dispatch(setZip(zip));
    dispatch(getSearchResults(zip));
  },
  callLoadingSearchResults: (status) => {
    dispatch(setLoadingSearchResults(status));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(IndexTemplate);
