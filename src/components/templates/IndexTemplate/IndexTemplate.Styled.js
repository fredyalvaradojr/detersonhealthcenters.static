import styled from '@emotion/styled';

export const IndexTemplate = styled.div``;

export const FilterResultsContainer = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: 48em) {
    flex-direction: row;
    max-width: 59.375em;
    margin: 0 auto;
  }
`;

export const LoadSpinnerContainer = styled.div`
  text-align: center;
`;
