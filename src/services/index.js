import axiosInstance from '../util/AxiosInstance';

export const getDoctorResultsApi = () => {
  const axios = axiosInstance();

  return axios
    .get('doctors')
    .then(({ data }) => data)
    .catch((error) => {
      // would need real logging such as Rollbar or New relic
      console.log(error);
    });
};
