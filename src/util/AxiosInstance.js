import axios from 'axios';

const axiosInstance = () => {
  const baseURL = process.env.NODE_ENV === 'development'
    ? 'http://localhost:3001/'
    : 'https://det-helth-ui.herokuapp.com/';

  const axiosConfig = {
    baseURL,
    headers: { Accept: 'application/json' },
  };

  const instance = axios.create(axiosConfig);
  instance.defaults.headers.post['Content-Type'] = 'application/json';

  return instance;
};

export default axiosInstance;
