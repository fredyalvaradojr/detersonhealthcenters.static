export const getGenderData = callbackToggleGender => [
  {
    title: 'Male',
    value: 'male',
    group: 'gender',
    callback: callbackToggleGender,
  },
  {
    title: 'Female',
    value: 'female',
    group: 'gender',
    callback: callbackToggleGender,
  },
  {
    title: 'No Preference',
    value: 'no-preference',
    group: 'gender',
    callback: callbackToggleGender,
  },
];
