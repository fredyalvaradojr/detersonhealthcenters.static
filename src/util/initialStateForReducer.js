export const resultsInitState = {
  results: [],
  loadingResults: false,
  distance: '30',
  zip: '',
  gender: 'no-preference',
};
