export const returnSortList = (filterBy, list) => {
  if (filterBy === 'no-preference') return list;

  return list.filter((listItem) => {
    const genderToCheck = listItem.gender || 'no-preference';
    return genderToCheck.toLowerCase() === filterBy;
  });
};
