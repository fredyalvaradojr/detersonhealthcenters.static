export const returnFilterList = (filterBy, list) => {
  const filterByNum = parseFloat(filterBy);
  const listFilteredByDistance = list.sort(
    (a, b) => a.locations[0].distance - b.locations[0].distance,
  );

  if (filterByNum < 30) {
    const rangeDistanceFilteredList = listFilteredByDistance.filter(
      doctorCard => doctorCard.locations[0].distance <= filterByNum,
    );

    return rangeDistanceFilteredList;
  }

  return listFilteredByDistance;
};
