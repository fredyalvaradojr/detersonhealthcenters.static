export const SET_DISTANCE = 'SET_DISTANCE';
export const setDistance = distance => ({ type: SET_DISTANCE, distance });

export const SET_ZIP = 'SET_ZIP';
export const setZip = zip => ({ type: SET_ZIP, zip });
