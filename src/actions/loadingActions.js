export const SET_LOADING_SEARCH_RESULTS = 'SET_LOADING_SEARCH_RESULTS';
export const setLoadingSearchResults = status => ({ type: SET_LOADING_SEARCH_RESULTS, status });
