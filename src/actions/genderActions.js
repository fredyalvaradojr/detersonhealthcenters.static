export const SET_GENDER = 'SET_GENDER';
export const setGender = gender => ({ type: SET_GENDER, gender });
