export const LOAD_SEARCH_RESULTS_SUCCEED = 'LOAD_RESULTS_SUCCEED';
export const loadSearchResultsSucceed = results => ({ type: LOAD_SEARCH_RESULTS_SUCCEED, results });

export const GET_SEARCH_RESULTS = 'GET_SEARCH_RESULTS';
export const getSearchResults = zip => ({ type: GET_SEARCH_RESULTS, zip });
