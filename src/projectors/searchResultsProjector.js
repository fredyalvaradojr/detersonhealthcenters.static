import {
  searchResultsSelector,
  filterByDistanceSelector,
  sortByGenderSelector,
} from './../selectors';
import { returnFilterList, returnSortList } from '../util';

export const searchResultsProjector = (state) => {
  const searchResultList = searchResultsSelector(state);
  const filterByDistance = filterByDistanceSelector(state);
  const distanceSearchResultList = returnFilterList(filterByDistance, searchResultList);
  const sortByGender = sortByGenderSelector(state);
  const distanceSortResultList = returnSortList(sortByGender, distanceSearchResultList);

  return distanceSortResultList;
};
