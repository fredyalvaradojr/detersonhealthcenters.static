module.exports = {
  env: {
    browser: true,
    es6: true,
    jest: true,
  },
  extends: 'airbnb',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    'no-param-reassign': ['error', { props: false }],
    'jsx-a11y/label-has-associated-control': [0],
    'jsx-a11y/label-has-for': [0],
    'react/forbid-prop-types': [0],
    'import/prefer-default-export': [0],
    'no-console': [0],
    'implicit-arrow-linebreak': [0],
    'import/no-useless-path-segments': [0],
    'react/jsx-filename-extension': [
      1,
      {
        extensions: ['.js', '.jsx'],
      },
    ],
  },
};
